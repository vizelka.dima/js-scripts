class GameBoard{
	constructor(size, color){
		this.size = size
		this.board = document.createElement('div')
		this.board.style.width = size + 'px'
		this.board.style.height = size + 'px'
		this.board.style.backgroundColor = color
	}

	add(circle){
		this.board.append(circle)
	}

	clear(){
		this.board.innerHTML = ""
	}
}

class Score{
	constructor(elem, winScore){
		this.element = elem
		this.current = 0
		this.winScore = winScore
	}

	isWin(){
		return this.current >= this.winScore
	}

	reset(){
		this.update(0)
	}

	inc(val){
		this.update(this.current + val)
	}

	update(val){
		this.current = (val > this.winScore)? this.winScore : val
		this.element.textContent = this.current
	}
}

class ControlPanel{
	constructor(pause, reset){
		this.pause = this.createButton('Pause')
		this.pause.addEventListener('click', pause)

		this.reset = this.createButton('Reset')
		this.reset.addEventListener('click', reset)
	}

	createButton(title){
		const btn = document.createElement('button')
		btn.textContent = title
		document.body.appendChild(btn)
		return btn
	}
}

class Timer{
	constructor(update, delay){
		this.update = update
		this.delay = delay
		this.running = false
		this.timeoutRunning = false
	} 

	start(){
		this.running = true
		this.startTime = Date.now()
		this.update()
		this.interval = window.setInterval(this.update, this.delay)
	}

	pause(){
		this.running = false
		this.pauseTime = Date.now()
		this.remainingTime = this.delay - ((Date.now() - this.startTime) % this.delay)

		if(this.timeoutRunning)
			window.clearTimeout(this.timeout)
		else
			window.clearInterval(this.interval)
	}

	unpause(){
		this.running = true
		this.startTime += (Date.now() - this.pauseTime) 
		this.timeout = window.setTimeout(this.afterUnpause.bind(this), this.remainingTime);
		this.timeoutRunning = true
	}


	afterUnpause(){
		this.update()
		this.interval = window.setInterval(this.update, this.delay)
		this.timeoutRunning = false
	}

	reset(){
		window.clearInterval(this.interval)
		this.start()
	}

	get duration(){
		return Date.now() - this.startTime
	}
}

class Clicker
{
	constructor(size, maxWeight, delay, winScore){
		const scoreElement = document.getElementById('score-value')
		this.score = new Score(scoreElement, winScore)

		this.circleFactory = new CircleFactory(size, maxWeight, this.clickHandler.bind(this))
		this.gameBoard = new GameBoard(size, '#f8f8f8')

		this.control = new ControlPanel(this.pause.bind(this), this.reset.bind(this))
		this.timer = new Timer(this.update.bind(this), delay)
		
		this.circle = this.updateCircle()
	}


	renderTo(element){
		this.gameBoard.add(this.circle.element)
		element.appendChild(this.gameBoard.board)

		this.timer.start()
	}

	update() {
		this.circle.delete()
		this.circle = this.updateCircle()
		this.gameBoard.add(this.circle.element)
	}

	updateCircle(){
		return this.circleFactory.generateCircle()
	}

	clickHandler(circle){
		if(this.timer.running){
			this.score.inc(circle.points)
			if(this.score.isWin()){
				this.win()
				return true
			}
		}
		return this.timer.running
	}

	win(){
		this.control.pause.disabled = true
		this.timer.pause()
		this.score.element.textContent = `You won (${this.score.current} points), it took you ${this.timer.duration / 1000} seconds`
	}

	
	reset(e){
		this.control.pause.disabled = false
		this.timer.reset()
		this.score.reset()
		this.control.pause.textContent = "Pause"
	}

	pause(e){
		const btn = e.target
		if(this.timer.running){
			this.timer.pause()
			btn.textContent = "Unpause"
		}
		else{
			this.timer.unpause()
			btn.textContent = "Pause"
		}
	}
}

class Circle{
	constructor(size, points, posX, posY, clickHandler){
		this.size = size
		this.points = points
		this.element = document.createElement('div')
		this.element.textContent = points
		this.element.classList.add('cell')
		this.element.style.width = size + 'px'
		this.element.style.height = size + 'px'		
		this.element.style.marginLeft = posX + 'px'
		this.element.style.marginTop = posY + 'px'
		this.element.style.lineHeight = (size) + 'px'

		this.clickMainHandler = clickHandler
		this.clickHandlerBinded = this.clickHandler.bind(this)
		this.element.addEventListener('click', this.clickHandlerBinded)

	}

	clickHandler(){
		if(this.clickMainHandler(this) && this.element){
			this.element.removeEventListener('click', this.clickHandlerBinded)
			this.element.style.backgroundColor = 'orange'
		}
	}

	delete(){
		this.element.remove();
	}

}

class CircleFactory{
	constructor(boardSize, maxPoints, clickHandler){
		this.boardSize = boardSize
		this.maxPoints = maxPoints
		this.clickHandler = clickHandler
	}

	getRndPos(offset) {
		return (Math.random() * (this.boardSize - offset))
	}

	generateCircle(){
		const points = Math.round(Math.random() * (this.maxPoints - 1)) + 1 
		const size = ((this.boardSize / 7) / this.maxPoints) * ((this.maxPoints + 1) - points)
		
		const posX = this.getRndPos(size)
		const posY = this.getRndPos(size)
		const circle = new Circle(size,points,posX, posY, this.clickHandler)
		
		return circle
	}
}

const clicker = new Clicker(400, 5, 1500, 10)
clicker.renderTo(document.body)

