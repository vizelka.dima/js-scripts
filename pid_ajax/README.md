# Instrukce k vypracování úlohy

1. Vytvořte si novou branch z větve `master` (nazvěte ji, jak chcete)
2. Do této větve můžete postupně commitovat Vaše řešení
3. Až budete s řešním spokojeni (nebo se přiblíží deadline), vytvořte z této nové branch `merge request` do `master`, a přiřaďte jako `Assignee` Oldu
4. Termín odevzdání je **2. 5. 2021 23:59:59** - za čas odevzdání se považuje přiřazení Merge requestu cvičícímu.

---

### Vypisování zastávek PID

* Kompletní zadání je součástí souboru `index.html` - doporučuji otevřít jej v browseru, nečíst zadání ze zdrojáku,
* **index.html vůbec neměňte**,
* řešení vypracujte do souboru `main.js`.