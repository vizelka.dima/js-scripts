class PidViewer{
	constructor(url, solutionElem, loadBtn){
		this.url = url
		this.loadBtn = loadBtn
		this.solutionBlock = solutionElem

		//Search form elements
		this.searchInput = this.searchInput()
		this.searchBtn = this.searchBtn()
		this.searchResult = document.createElement('div')
		
		//Search form
		this.searchForm = document.createElement('div')
		this.searchForm.append(this.searchInput, this.searchBtn, this.searchResult)
		this.searchFormVisible = false

		this.addCustomStyles()
	}

	addCustomStyles(){
		//Create Style for multiple search item titles
		const style = document.createElement('style')
		style.textContent = 
		`	
			div.multipleSearchItemTitle{
				cursor: pointer;
				text-decoration: underline;
				padding-bottom: 7px
			}	
			
			div.multipleSearchItemTitle:hover{
				color: blue
			}

			div.searchDetail{
				margin-bottom: 10px
			}
		`
		document.body.append(style)
	}


	//Create search input
	searchInput(){
		const sin = document.createElement('input')
		sin.placeholder = 'Hledat'
		return sin
	}

	//Create search button
	searchBtn(){
		const sBtn = document.createElement('button')
		sBtn.textContent = 'Hledej!'
		sBtn.disabled = true
		return sBtn
	}

	//Set event listeners & bind them to PidViewer instance (=this)
	start(){
		this.loadBtn.addEventListener('click', this.loadData.bind(this))
		this.searchInput.addEventListener('input', this.inputHandler.bind(this))
		this.searchBtn.addEventListener('click', this.search.bind(this))
		this.searchBtn.addEventListener('keypress', e => e.preventDefault()) //disable default keypress on search button
		document.addEventListener('keydown', this.searchByKey.bind(this))	 //set keydown event on document
	}

	//toggle search button ( no input -> disable )
	inputHandler(){
		this.searchBtn.disabled = (this.searchInput.value === "")
	}

	//search using 'Enter' key
	searchByKey(e){
		if(!this.searchBtn.disabled && e.key === 'Enter')
			this.search()
	}

	search(){
		//clear search results
		this.searchResult.innerHTML = ""; 

		const searchVal = this.searchInput.value
		const results = []

		const values = this.data["stopGroups"]

		let i = 0
		values.forEach((val) => {
			if(val["name"].includes(searchVal))
				results.push(i)
				++i
		})
		
		const wrapper = document.createDocumentFragment()

		if (results.length > 1) { //More results -> set showDetails on click event
			const h = document.createElement('h3')
			h.textContent = 'Měli jste na mysli:'
			wrapper.appendChild(h)

			results.forEach((i)=>{
				const title = document.createElement("div")
				title.classList.add('multipleSearchItemTitle')
				title.innerHTML = `${values[i]["name"]}` 
				title.addEventListener('click', () => {this.showDetails(i)})
				wrapper.appendChild(title)
			})
		}
		else if(results.length === 1){ //One result -> show details 
			this.showDetails(results[0])
		}
		else	//No result
		{
			const h = document.createElement('h3')
			h.textContent = 'Žádné výsledky!'
			wrapper.appendChild(h)
		}

		this.searchResult.append(wrapper)
	}

	showDetails(index){
		this.searchResult.innerHTML = "";
		const wrapper = document.createDocumentFragment()
		
		const resDetail = this.data["stopGroups"][index]
		
		const name = document.createElement('h3')
		name.textContent = resDetail['name']
		wrapper.appendChild(name)

		const stops = resDetail["stops"]
		
		let station = 1
		stops.forEach((stop)=>{
			const block = document.createElement("div")
			block.classList.add('searchDetail')

			const title = document.createElement("a")
			title.setAttribute('href', `https://www.google.com/maps/place/${stop["lat"]},${stop["lon"]}`)
			title.setAttribute('target', '_blank')
			title.innerHTML = "Nástupiště " + station
			block.appendChild(title)

			stop["lines"].forEach((t)=>{
				const li = document.createElement("li")
				li.innerHTML = `${t["type"]} ${t["name"]} ➡ ${t["direction"]}`
				block.appendChild(li)
			})

			wrapper.appendChild(block)
			++station
		})

		this.searchResult.append(wrapper)
	}

	checkData(response){
		if (response.status >= 200 && response.status <= 299)
			return response.json();
		else 
			throw Error(response.statusText);
	}

	loadData(){
		this.loadBtn.disabled = true
		fetch(this.url)
			.then(this.checkData)
			.then(data => { 
				this.data = data

				//Display search form and change load button text ONLY ONCE
				if(!this.searchFormVisible) {
					this.loadBtn.textContent = 'Aktualizovat data o zastávkách'
					this.showSearchForm()
				}

				this.loadBtn.disabled = false
			})
			.catch((error) => {
				throw Error("Can not get data | parse json")
			}) 
	}

	showSearchForm(){
		this.solutionBlock.appendChild(this.searchForm)
	}
}

const solution = document.getElementById('solution')
const loadBtn = document.getElementById('loadData')
const app = new PidViewer('https://gist.githubusercontent.com/oldamalec/161956587b930cd8aa3628c2da24579f/raw/0132aa3624141745862e55f48defc55b007b02cb/opendata-pid-stops.js',
						   solution,
						   loadBtn)
app.start()
